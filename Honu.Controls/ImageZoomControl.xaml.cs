﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Honu.Controls
{
    /// <summary>
    /// Interaction logic for ImageZoomControl.xaml
    /// </summary>
    public partial class ImageZoomControl : UserControl, IImageZoomControl
    {
        /// <summary>
        /// Drag threshold
        /// </summary>
        public const int DragThreshold = 5;

        /// <summary>
        /// The image source property
        /// </summary>
        public static readonly DependencyProperty ImageSourceProperty = DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(ImageZoomControl), new PropertyMetadata(null));
        
        /// <summary>
        /// The interaction mode property
        /// </summary>
        public static readonly DependencyProperty InteractionModeProperty = DependencyProperty.Register("InteractionMode", typeof(MouseHandlingMode), typeof(ImageZoomControl), new PropertyMetadata(MouseHandlingMode.ZoomingIn));
        
        /// <summary>
        /// The show controls property
        /// </summary>
        public static readonly DependencyProperty ShowControlsProperty = DependencyProperty.Register("ShowControls", typeof(bool), typeof(ImageZoomControl), new PropertyMetadata(true));
        
        /// <summary>
        /// The toolbar background property
        /// </summary>
        public static readonly DependencyProperty ToolbarBackgroundProperty = DependencyProperty.Register("ToolbarBackground", typeof(Brush), typeof(ImageZoomControl), new PropertyMetadata(Brushes.LightGray));
        
        /// <summary>
        /// The zoom rectangle background property
        /// </summary>
        public static readonly DependencyProperty ZoomRectangleBackgroundProperty = DependencyProperty.Register("ZoomRectangleBackground", typeof(Brush), typeof(ImageZoomControl), new PropertyMetadata(Brushes.LightCyan));
        
        /// <summary>
        /// The zoom rectangle border brush property
        /// </summary>
        public static readonly DependencyProperty ZoomRectangleBorderBrushProperty = DependencyProperty.Register("ZoomRectangleBorderBrush", typeof(Brush), typeof(ImageZoomControl), new PropertyMetadata(Brushes.Blue));
        
        /// <summary>
        /// The zoom rectangle opacity property
        /// </summary>
        public static readonly DependencyProperty ZoomRectangleOpacityProperty = DependencyProperty.Register("ZoomRectangleOpacity", typeof(double), typeof(ImageZoomControl), new PropertyMetadata(0.5));

        /// <summary>
        /// Indicates if this control is currently dragging a zoom rectangle.
        /// </summary>
        private bool isDragging;

        /// <summary>
        /// The point that was clicked relative to the ZoomAndPanControl.
        /// </summary>
        private Point originalControlPoint;

        /// <summary>
        /// The point that was clicked relative to the content that is contained within the ZoomAndPanControl.
        /// </summary>
        private Point originalImagePoint;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageZoomControl"/> class.
        /// </summary>
        public ImageZoomControl()
        { this.InitializeComponent(); }

        /// <summary>
        /// Gets or sets the image source.
        /// </summary>
        public ImageSource ImageSource
        {
            get { return this.GetValue(ImageSourceProperty) as ImageSource; }
            set { this.SetValue(ImageSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the interaction mode.
        /// </summary>
        public MouseHandlingMode InteractionMode
        {
            get { return (MouseHandlingMode)this.GetValue(InteractionModeProperty); }
            set { this.SetValue(InteractionModeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the control.
        /// </summary>
        public bool ShowControls
        {
            get { return (bool)this.GetValue(ShowControlsProperty); }
            set { this.SetValue(ShowControlsProperty, value); }
        }

        /// <summary>
        /// Gets or sets the toolbar background.
        /// </summary>
        public Brush ToolbarBackground
        {
            get { return (Brush)this.GetValue(ToolbarBackgroundProperty); }
            set { this.SetValue(ToolbarBackgroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the zoom rectangle background.
        /// </summary>
        public Brush ZoomRectangleBackground
        {
            get { return (Brush)this.GetValue(ZoomRectangleBackgroundProperty); }
            set { this.SetValue(ZoomRectangleBackgroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the zoom rectangle border brush.
        /// </summary>
        public Brush ZoomRectangleBorderBrush
        {
            get { return (Brush)this.GetValue(ZoomRectangleBorderBrushProperty); }
            set { this.SetValue(ZoomRectangleBorderBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets the zoom rectangle opacity.
        /// </summary>
        public double ZoomRectangleOpacity
        {
            get { return (double)this.GetValue(ZoomRectangleOpacityProperty); }
            set { this.SetValue(ZoomRectangleOpacityProperty, value); }
        }

        /// <summary>
        /// Resets the view to actual size.
        /// </summary>
        public void ResetView()
        { this.zoomControl.AnimatedZoomTo(1.0); }

        /// <summary>
        /// Scales the image to fit the extent
        /// </summary>
        public void ScaleToFit()
        { this.zoomControl.AnimatedScaleToFit(); }

        /// <summary>
        /// When the user has finished dragging out the rectangle the zoom operation is applied.
        /// </summary>
        private void ApplyDragZoomRect()
        {
            // Retrieve the rectangle that the user dragged out and zoom in on it.
            double contentX = Canvas.GetLeft(this.border);
            double contentY = Canvas.GetTop(this.border);
            double contentWidth = this.border.Width;
            double contentHeight = this.border.Height;
            this.zoomControl.AnimatedZoomTo(new Rect(contentX, contentY, contentWidth, contentHeight));
            this.FadeOutDragZoomRect();
        }

        /// <summary>
        /// Fade out the drag zoom rectangle.
        /// </summary>
        private void FadeOutDragZoomRect()
        {
            this.border.StartAnimation(
                Border.OpacityProperty,
                0.0,
                0.1,
                (o, e) => { canvas.Visibility = Visibility.Collapsed; });
        }

        /// <summary>
        /// Handles the Executed event of the Fill command.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void Fill_Executed(object sender, ExecutedRoutedEventArgs e)
        { this.ScaleToFit(); }

        /// <summary>
        /// Initialize the rectangle that the use is dragging out.
        /// </summary>
        /// <param name="pt1">Point one</param>
        /// <param name="pt2">Point two</param>
        private void InitDragZoomRect(Point pt1, Point pt2)
        {
            this.SetDragZoomRect(pt1, pt2);
            this.canvas.Visibility = Visibility.Visible;
            this.border.Opacity = this.ZoomRectangleOpacity;
        }

        /// <summary>
        /// Called when the scale command is executed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void OneHundredPercent_Executed(object sender, ExecutedRoutedEventArgs e)
        { this.ResetView(); }

        /// <summary>
        /// Update the position and size of the rectangle that user is dragging out.
        /// </summary>
        /// <param name="pt1">Point one</param>
        /// <param name="pt2">Point two</param>
        private void SetDragZoomRect(Point pt1, Point pt2)
        {
            // Calculate coordinates
            double minX = Math.Min(pt1.X, pt2.X);
            double maxX = Math.Max(pt1.X, pt2.X);
            double minY = Math.Min(pt1.Y, pt2.Y);
            double maxY = Math.Max(pt1.Y, pt2.Y);

            // Update the coordinates of the rectangle that is being dragged out by the user.
            // Then offset and rescale to convert from content coordinates.
            Canvas.SetLeft(this.border, minX);
            Canvas.SetTop(this.border, minY);
            this.border.Width = maxX - minX;
            this.border.Height = maxY - minY;
        }

        /// <summary>
        /// Handles the MouseDoubleClick event of the ZoomAndPanControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ZoomAndPanControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Point point = e.GetPosition(image);
            this.zoomControl.AnimatedSnapTo(point);
        }

        /// <summary>
        /// Handles the MouseDown event of the ZoomAndPanControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ZoomAndPanControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.image.Focus();
            Keyboard.Focus(this.image);

            this.originalControlPoint = e.GetPosition(this.zoomControl);
            this.originalImagePoint = e.GetPosition(this.image);

            if (this.InteractionMode != MouseHandlingMode.None)
            {
                // Capture the mouse so that we eventually receive the mouse up event.
                this.zoomControl.CaptureMouse();
                e.Handled = true;
            }
        }

        /// <summary>
        /// Handles the MouseMove event of the ZoomAndPanControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void ZoomAndPanControl_MouseMove(object sender, MouseEventArgs e)
        {
            // Only react to left mouse button
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Point currentImagePoint = e.GetPosition(this.image);

                if (this.InteractionMode == MouseHandlingMode.Panning)
                {
                    // Pan the control!
                    Vector delta = currentImagePoint - this.originalImagePoint;
                    this.zoomControl.ContentOffsetX -= delta.X;
                    this.zoomControl.ContentOffsetY -= delta.Y;

                    e.Handled = true;
                }
                else if (this.InteractionMode == MouseHandlingMode.DragZooming)
                {
                    Point currentControlPoint = e.GetPosition(this.zoomControl);

                    if (this.isDragging)
                    {
                        // Recalculate zoom area
                        this.SetDragZoomRect(this.originalImagePoint, currentImagePoint);
                        e.Handled = true;
                    }
                    else
                    {
                        // Check if distance is beyond drag threshold to initiate zoom area
                        Vector delta = currentControlPoint - this.originalControlPoint;
                        double distance = Math.Abs(delta.Length);

                        if (distance > DragThreshold)
                        {
                            this.isDragging = true;
                            this.InitDragZoomRect(this.originalImagePoint, currentImagePoint);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handles the MouseUp event of the ZoomAndPanControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ZoomAndPanControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (this.InteractionMode != MouseHandlingMode.None)
            {
                switch (this.InteractionMode)
                {
                    case MouseHandlingMode.ZoomingIn:
                        this.ZoomIn(this.originalImagePoint);
                        break;

                    case MouseHandlingMode.ZoomingOut:
                        this.ZoomOut(this.originalImagePoint);
                        break;

                    case MouseHandlingMode.DragZooming:
                        this.ApplyDragZoomRect();
                        this.isDragging = false;
                        break;
                }

                this.zoomControl.ReleaseMouseCapture();
                e.Handled = true;
            }
        }

        /// <summary>
        /// Handles the MouseWheel event of the ZoomAndPanControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseWheelEventArgs"/> instance containing the event data.</param>
        private void ZoomAndPanControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
            Point point = e.GetPosition(this.image);

            if (e.Delta > 0)
            { this.ZoomIn(point); }
            else if (e.Delta < 0)
            { this.ZoomOut(point); }
        }

        /// <summary>
        /// Zoom the viewport in, centering on the specified point (in content coordinates).
        /// </summary>
        /// <param name="contentZoomCenter">Center point</param>
        private void ZoomIn(Point contentZoomCenter)
        { this.zoomControl.ZoomAboutPoint(this.zoomControl.ContentScale + 0.2, contentZoomCenter); }

        /// <summary>
        /// Handles the Executed event of the ZoomIn command.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void ZoomIn_Executed(object sender, ExecutedRoutedEventArgs e)
        { this.ZoomIn(new Point(this.zoomControl.ContentZoomFocusX, this.zoomControl.ContentZoomFocusY)); }

        /// <summary>
        /// Zoom the viewport out, centering on the specified point (in content coordinates).
        /// </summary>
        /// <param name="contentZoomCenter">Center point</param>
        private void ZoomOut(Point contentZoomCenter)
        { this.zoomControl.ZoomAboutPoint(this.zoomControl.ContentScale - 0.2, contentZoomCenter); }

        /// <summary>
        /// Handles the Executed event of the ZoomOut command.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void ZoomOut_Executed(object sender, ExecutedRoutedEventArgs e)
        { this.ZoomOut(new Point(this.zoomControl.ContentZoomFocusX, this.zoomControl.ContentZoomFocusY)); }
    }
}
