﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace Honu.Controls
{
    /// <summary>
    /// A class that wraps up zooming and panning of its content - used as part of the <see cref="ImageZoomControl"/>
    /// </summary>
    public class ZoomAndPanControl : ContentControl, IScrollInfo
    {
        /// <summary>
        /// The animation duration property
        /// </summary>
        public static readonly DependencyProperty AnimationDurationProperty = DependencyProperty.Register("AnimationDuration", typeof(double), typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(0.4));

        /// <summary>
        /// The content X offset property
        /// </summary>
        public static readonly DependencyProperty ContentOffsetXProperty = DependencyProperty.Register("ContentOffsetX", typeof(double), typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(0.0, ContentOffset_PropertyChanged, ContentOffsetX_Coerce));

        /// <summary>
        /// The content Y offset property
        /// </summary>
        public static readonly DependencyProperty ContentOffsetYProperty = DependencyProperty.Register("ContentOffsetY", typeof(double), typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(0.0, ContentOffset_PropertyChanged, ContentOffsetY_Coerce));

        /// <summary>
        /// The content scale property
        /// </summary>
        public static readonly DependencyProperty ContentScaleProperty = DependencyProperty.Register("ContentScale", typeof(double), typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(1.0, ContentScale_PropertyChanged, ContentScale_Coerce));

        /// <summary>
        /// The content viewport height property
        /// </summary>
        public static readonly DependencyProperty ContentViewportHeightProperty = DependencyProperty.Register("ContentViewportHeight", typeof(double), typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(0.0));

        /// <summary>
        /// The content viewport width property
        /// </summary>
        public static readonly DependencyProperty ContentViewportWidthProperty = DependencyProperty.Register("ContentViewportWidth", typeof(double), typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(0.0));

        /// <summary>
        /// The content X zoom focus property
        /// </summary>
        public static readonly DependencyProperty ContentZoomFocusXProperty = DependencyProperty.Register("ContentZoomFocusX", typeof(double), typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(0.0));

        /// <summary>
        /// The content Y zoom focus property
        /// </summary>
        public static readonly DependencyProperty ContentZoomFocusYProperty = DependencyProperty.Register("ContentZoomFocusY", typeof(double), typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(0.0));

        /// <summary>
        /// The Is mouse wheel scrolling enabled property
        /// </summary>
        public static readonly DependencyProperty IsMouseWheelScrollingEnabledProperty = DependencyProperty.Register("IsMouseWheelScrollingEnabled", typeof(bool), typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(false));

        /// <summary>
        /// The maximum content scale property
        /// </summary>
        public static readonly DependencyProperty MaxContentScaleProperty = DependencyProperty.Register("MaxContentScale", typeof(double), typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(10.0, MinOrMaxContentScale_PropertyChanged));

        /// <summary>
        /// The minimum content scale property
        /// </summary>
        public static readonly DependencyProperty MinContentScaleProperty = DependencyProperty.Register("MinContentScale", typeof(double), typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(0.01, MinOrMaxContentScale_PropertyChanged));

        /// <summary>
        /// The viewport zoom focus x property
        /// </summary>
        public static readonly DependencyProperty ViewportZoomFocusXProperty = DependencyProperty.Register("ViewportZoomFocusX", typeof(double), typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(0.0));

        /// <summary>
        /// The viewport zoom focus y property
        /// </summary>
        public static readonly DependencyProperty ViewportZoomFocusYProperty = DependencyProperty.Register("ViewportZoomFocusY", typeof(double), typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(0.0));

        /// <summary>
        /// Set to 'true' when the vertical scrollbar is enabled.
        /// </summary>
        private bool canHorizontallyScroll = false;

        /// <summary>
        /// Set to 'true' when the vertical scrollbar is enabled.
        /// </summary>
        private bool canVerticallyScroll = false;

        /// <summary>
        /// The height of the viewport in content coordinates, clamped to the height of the content.
        /// </summary>
        private double constrainedContentViewportHeight = 0.0;

        /// <summary>
        /// The width of the viewport in content coordinates, clamped to the width of the content.
        /// </summary>
        private double constrainedContentViewportWidth = 0.0;

        /// <summary>
        /// Reference to the underlying content, which is named PART_Content in the template.
        /// </summary>
        private FrameworkElement content = null;

        /// <summary>
        /// The transform that is applied to the content to offset it by 'ContentOffsetX' and 'ContentOffsetY'.
        /// </summary>
        private TranslateTransform contentOffsetTransform = null;

        /// <summary>
        /// The transform that is applied to the content to scale it by 'ContentScale'.
        /// </summary>
        private ScaleTransform contentScaleTransform = null;

        /// <summary>
        /// Normally when content offsets changes the content focus is automatically updated.
        /// This synchronization is disabled when 'disableContentFocusSync' is set to 'true'.
        /// When we are zooming in or out we 'disableContentFocusSync' is set to 'true' because
        /// we are zooming in or out relative to the content focus we don't want to update the focus.
        /// </summary>
        private bool disableContentFocusSync = false;

        /// <summary>
        /// Used to disable synchronization between IScrollInfo interface and ContentOffsetX/ContentOffsetY.
        /// </summary>
        private bool disableScrollOffsetSync = false;

        /// <summary>
        /// Enable the update of the content offset as the content scale changes.
        /// This enabled for zooming about a point (Google Maps style zooming) and zooming to a rectangle.
        /// </summary>
        private bool enableContentOffsetUpdateFromScale = false;

        /// <summary>
        /// Reference to the ScrollViewer that is wrapped (in XAML) around the ZoomAndPanControl.
        /// Or set to null if there is no ScrollViewer.
        /// </summary>
        private ScrollViewer scrollOwner = null;

        /// <summary>
        /// Records the un-scaled extent of the content.
        /// This is calculated during the measure and arrange.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed.")]
        private Size unScaledExtent = new Size(0, 0);

        /// <summary>
        /// Records the size of the viewport (in viewport coordinates) onto the content.
        /// This is calculated during the measure and arrange.
        /// </summary>
        private Size viewport = new Size(0, 0);

        /// <summary>
        /// Initializes static members of the <see cref="ZoomAndPanControl"/> class.
        /// </summary>
        static ZoomAndPanControl()
        { DefaultStyleKeyProperty.OverrideMetadata(typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(typeof(ZoomAndPanControl))); }

        /// <summary>
        /// Event raised when the ContentOffsetX property has changed.
        /// </summary>
        public event EventHandler ContentOffsetXChanged;

        /// <summary>
        /// Event raised when the ContentOffsetY property has changed.
        /// </summary>
        public event EventHandler ContentOffsetYChanged;

        /// <summary>
        /// Event raised when the ContentScale property has changed.
        /// </summary>
        public event EventHandler ContentScaleChanged;

        /// <summary>
        /// Gets or sets the duration of the animations (in seconds) started by calling AnimatedZoomTo and the other animation methods.
        /// </summary>
        public double AnimationDuration
        {
            get { return (double)this.GetValue(AnimationDurationProperty); }
            set { this.SetValue(AnimationDurationProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the vertical scrollbar is enabled.
        /// </summary>
        public bool CanHorizontallyScroll
        {
            get { return this.canHorizontallyScroll; }
            set { this.canHorizontallyScroll = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the vertical scrollbar is enabled.
        /// </summary>
        public bool CanVerticallyScroll
        {
            get { return this.canVerticallyScroll; }
            set { this.canVerticallyScroll = value; }
        }

        /// <summary>
        /// Gets or sets the X offset (in content coordinates) of the view on the content.
        /// </summary>
        public double ContentOffsetX
        {
            get { return (double)this.GetValue(ContentOffsetXProperty); }
            set { this.SetValue(ContentOffsetXProperty, value); }
        }

        /// <summary>
        /// Gets or sets the Y offset (in content coordinates) of the view on the content.
        /// </summary>
        public double ContentOffsetY
        {
            get { return (double)this.GetValue(ContentOffsetYProperty); }
            set { this.SetValue(ContentOffsetYProperty, value); }
        }

        /// <summary>
        /// Gets or sets the current scale (or zoom factor) of the content.
        /// </summary>
        public double ContentScale
        {
            get { return (double)this.GetValue(ContentScaleProperty); }
            set { this.SetValue(ContentScaleProperty, value); }
        }

        /// <summary>
        /// Gets or sets the viewport height, in content coordinates.
        /// </summary>
        public double ContentViewportHeight
        {
            get { return (double)this.GetValue(ContentViewportHeightProperty); }
            set { this.SetValue(ContentViewportHeightProperty, value); }
        }

        /// <summary>
        /// Gets or sets the viewport width, in content coordinates.
        /// </summary>
        public double ContentViewportWidth
        {
            get { return (double)this.GetValue(ContentViewportWidthProperty); }
            set { this.SetValue(ContentViewportWidthProperty, value); }
        }

        /// <summary>
        /// Gets or sets the X coordinate of the content focus, this is the point that we are focusing on when zooming.
        /// </summary>
        public double ContentZoomFocusX
        {
            get { return (double)this.GetValue(ContentZoomFocusXProperty); }
            set { this.SetValue(ContentZoomFocusXProperty, value); }
        }

        /// <summary>
        /// Gets or sets the Y coordinate of the content focus, this is the point that we are focusing on when zooming.
        /// </summary>
        public double ContentZoomFocusY
        {
            get { return (double)this.GetValue(ContentZoomFocusYProperty); }
            set { this.SetValue(ContentZoomFocusYProperty, value); }
        }

        /// <summary>
        /// Gets the height of the content (with 'ContentScale' applied).
        /// </summary>
        public double ExtentHeight
        { get { return this.unScaledExtent.Height * this.ContentScale; } }

        /// <summary>
        /// Gets the width of the content (with 'ContentScale' applied).
        /// </summary>
        public double ExtentWidth
        { get { return this.unScaledExtent.Width * this.ContentScale; } }

        /// <summary>
        /// Gets the offset of the horizontal scrollbar.
        /// </summary>
        public double HorizontalOffset
        { get { return this.ContentOffsetX * this.ContentScale; } }

        /// <summary>
        /// Gets or sets a value indicating whether mouse wheel scrolling is enabled
        /// </summary>
        public bool IsMouseWheelScrollingEnabled
        {
            get { return (bool)this.GetValue(IsMouseWheelScrollingEnabledProperty); }
            set { this.SetValue(IsMouseWheelScrollingEnabledProperty, value); }
        }

        /// <summary>
        /// Gets or sets the maximum value for 'ContentScale'.
        /// </summary>
        public double MaxContentScale
        {
            get { return (double)this.GetValue(MaxContentScaleProperty); }
            set { this.SetValue(MaxContentScaleProperty, value); }
        }

        /// <summary>
        /// Gets or sets the minimum value for 'ContentScale'.
        /// </summary>
        public double MinContentScale
        {
            get { return (double)this.GetValue(MinContentScaleProperty); }
            set { this.SetValue(MinContentScaleProperty, value); }
        }

        /// <summary>
        /// Gets or sets the reference to the ScrollViewer that is wrapped (in XAML) around the ZoomAndPanControl.
        /// </summary>
        public ScrollViewer ScrollOwner
        {
            get { return this.scrollOwner; }
            set { this.scrollOwner = value; }
        }

        /// <summary>
        /// Gets the offset of the vertical scrollbar.
        /// </summary>
        public double VerticalOffset
        { get { return this.ContentOffsetY * this.ContentScale; } }

        /// <summary>
        /// Gets the height of the viewport onto the content.
        /// </summary>
        public double ViewportHeight
        { get { return this.viewport.Height; } }

        /// <summary>
        /// Gets the width of the viewport onto the content.
        /// </summary>
        public double ViewportWidth
        { get { return this.viewport.Width; } }

        /// <summary>
        /// Gets or sets the X coordinate of the viewport focus
        /// </summary>
        /// <remarks>This is the point in the viewport (in viewport coordinates) that the content focus point is locked to while zooming in.</remarks>
        public double ViewportZoomFocusX
        {
            get { return (double)this.GetValue(ViewportZoomFocusXProperty); }
            set { this.SetValue(ViewportZoomFocusXProperty, value); }
        }

        /// <summary>
        /// Gets or sets the Y coordinate of the viewport focus
        /// </summary>
        /// <remarks>This is the point in the viewport (in viewport coordinates) that the content focus point is locked to while zooming in.</remarks>
        public double ViewportZoomFocusY
        {
            get { return (double)this.GetValue(ViewportZoomFocusYProperty); }
            set { this.SetValue(ViewportZoomFocusYProperty, value); }
        }

        /// <summary>
        /// Do animation that scales the content so that it fits completely in the control.
        /// </summary>
        public void AnimatedScaleToFit()
        {
            if (this.content == null)
            { throw new ApplicationException("PART_Content was not found in the ZoomAndPanControl visual template!"); }

            this.AnimatedZoomTo(new Rect(0, 0, this.content.ActualWidth, this.content.ActualHeight));
        }

        /// <summary>
        /// Use animation to center the view on the specified point (in content coordinates).
        /// </summary>
        /// <param name="contentPoint">Content point</param>
        public void AnimatedSnapTo(Point contentPoint)
        {
            double newX = contentPoint.X - (this.ContentViewportWidth / 2);
            double newY = contentPoint.Y - (this.ContentViewportHeight / 2);

            this.StartAnimation(ContentOffsetXProperty, newX, this.AnimationDuration);
            this.StartAnimation(ContentOffsetYProperty, newY, this.AnimationDuration);
        }

        /// <summary>
        /// Zoom in/out centered on the specified point (in content coordinates).
        /// The focus point is kept locked to it's on screen position (a la Google maps).
        /// </summary>
        /// <param name="newContentScale">Content scale</param>
        /// <param name="contentZoomFocus">Content focus</param>
        public void AnimatedZoomAboutPoint(double newContentScale, Point contentZoomFocus)
        {
            newContentScale = Math.Min(Math.Max(newContentScale, this.MinContentScale), this.MaxContentScale);

            this.CancelAnimation(ContentOffsetXProperty);
            this.CancelAnimation(ContentOffsetYProperty);
            this.CancelAnimation(ViewportZoomFocusXProperty);
            this.CancelAnimation(ViewportZoomFocusYProperty);

            this.ContentZoomFocusX = contentZoomFocus.X;
            this.ContentZoomFocusY = contentZoomFocus.Y;
            this.ViewportZoomFocusX = (this.ContentZoomFocusX - this.ContentOffsetX) * this.ContentScale;
            this.ViewportZoomFocusY = (this.ContentZoomFocusY - this.ContentOffsetY) * this.ContentScale;

            // When zooming about a point make updates to ContentScale also update content offset.
            this.enableContentOffsetUpdateFromScale = true;

            this.StartAnimation(
                ContentScaleProperty,
                newContentScale,
                this.AnimationDuration,
                (o, e) =>
                {
                    this.enableContentOffsetUpdateFromScale = false;
                    this.ResetViewportZoomFocus();
                });
        }

        /// <summary>
        /// Do an animated zoom to view a specific scale and rectangle (in content coordinates).
        /// </summary>
        /// <param name="newScale">New scale</param>
        /// <param name="contentRect">Content area</param>
        public void AnimatedZoomTo(double newScale, Rect contentRect)
        {
            this.AnimatedZoomPointToViewportCenter(
                newScale,
                new Point(contentRect.X + (contentRect.Width / 2), contentRect.Y + (contentRect.Height / 2)),
                delegate(object sender, EventArgs e)
                {
                    // At the end of the animation, ensure that we are snapped to the specified content offset.
                    // Due to zooming in on the content focus point and rounding errors, the content offset may
                    // be slightly off what we want at the end of the animation and this bit of code corrects it.
                    this.ContentOffsetX = contentRect.X;
                    this.ContentOffsetY = contentRect.Y;
                });
        }

        /// <summary>
        /// Do an animated zoom to the specified rectangle (in content coordinates).
        /// </summary>
        /// <param name="contentRect">Content area</param>
        public void AnimatedZoomTo(Rect contentRect)
        {
            double scaleX = this.ContentViewportWidth / contentRect.Width;
            double scaleY = this.ContentViewportHeight / contentRect.Height;
            double newScale = this.ContentScale * Math.Min(scaleX, scaleY);

            this.AnimatedZoomPointToViewportCenter(newScale, new Point(contentRect.X + (contentRect.Width / 2), contentRect.Y + (contentRect.Height / 2)), null);
        }

        /// <summary>
        /// Zoom in/out centered on the viewport center.
        /// </summary>
        /// <param name="contentScale">Content scale</param>
        public void AnimatedZoomTo(double contentScale)
        {
            Point zoomCenter = new Point(this.ContentOffsetX + (this.ContentViewportWidth / 2), this.ContentOffsetY + (this.ContentViewportHeight / 2));
            this.AnimatedZoomAboutPoint(contentScale, zoomCenter);
        }

        /// <summary>
        /// Shift the content offset one line down.
        /// </summary>
        public void LineDown()
        { this.ContentOffsetY += this.ContentViewportHeight / 10; }

        /// <summary>
        /// Shift the content offset one line left.
        /// </summary>
        public void LineLeft()
        { this.ContentOffsetX -= this.ContentViewportWidth / 10; }

        /// <summary>
        /// Shift the content offset one line right.
        /// </summary>
        public void LineRight()
        { this.ContentOffsetX += this.ContentViewportWidth / 10; }

        /// <summary>
        /// Shift the content offset one line up.
        /// </summary>
        public void LineUp()
        { this.ContentOffsetY -= this.ContentViewportHeight / 10; }

        /// <summary>
        /// Bring the specified rectangle to view.
        /// </summary>
        /// <param name="visual">The visual.</param>
        /// <param name="rectangle">The rectangle.</param>
        /// <returns>Visible area</returns>
        public Rect MakeVisible(Visual visual, Rect rectangle)
        {
            if (this.content.IsAncestorOf(visual))
            {
                Rect transformedRect = visual.TransformToAncestor(this.content).TransformBounds(rectangle);
                Rect viewportRect = new Rect(this.ContentOffsetX, this.ContentOffsetY, this.ContentViewportWidth, this.ContentViewportHeight);

                if (!transformedRect.Contains(viewportRect))
                {
                    double horizOffset = 0;
                    double vertOffset = 0;

                    // Move viewport left.
                    if (transformedRect.Left < viewportRect.Left)
                    { horizOffset = transformedRect.Left - viewportRect.Left; }
                    else if (transformedRect.Right > viewportRect.Right)
                    { horizOffset = transformedRect.Right - viewportRect.Right; }

                    // Want to move viewport up.
                    if (transformedRect.Top < viewportRect.Top)
                    { vertOffset = transformedRect.Top - viewportRect.Top; }
                    else if (transformedRect.Bottom > viewportRect.Bottom)
                    { vertOffset = transformedRect.Bottom - viewportRect.Bottom; }

                    this.SnapContentOffsetTo(new Point(this.ContentOffsetX + horizOffset, this.ContentOffsetY + vertOffset));
                }
            }

            return rectangle;
        }

        /// <summary>
        /// Don't handle mouse wheel input from the ScrollViewer, the mouse wheel is
        /// used for zooming in and out, not for manipulating the scrollbars.
        /// </summary>
        public void MouseWheelDown()
        {
            if (this.IsMouseWheelScrollingEnabled)
            { this.LineDown(); }
        }

        /// <summary>
        /// Don't handle mouse wheel input from the ScrollViewer, the mouse wheel is
        /// used for zooming in and out, not for manipulating the scrollbars.
        /// </summary>
        public void MouseWheelLeft()
        {
            if (this.IsMouseWheelScrollingEnabled)
            { this.LineLeft(); }
        }

        /// <summary>
        /// Don't handle mouse wheel input from the ScrollViewer, the mouse wheel is
        /// used for zooming in and out, not for manipulating the scrollbars.
        /// </summary>
        public void MouseWheelRight()
        {
            if (this.IsMouseWheelScrollingEnabled)
            { this.LineRight(); }
        }

        /// <summary>
        /// Don't handle mouse wheel input from the ScrollViewer, the mouse wheel is
        /// used for zooming in and out, not for manipulating the scrollbars.
        /// </summary>
        public void MouseWheelUp()
        {
            if (this.IsMouseWheelScrollingEnabled)
            { this.LineUp(); }
        }

        /// <summary>
        /// Called when a template has been applied to the control.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.content = this.Template.FindName("PART_Content", this) as FrameworkElement;
            if (this.content != null)
            {
                // Setup the transform on the content so that we can scale it by 'ContentScale'.
                this.contentScaleTransform = new ScaleTransform(this.ContentScale, this.ContentScale);

                // Setup the transform on the content so that we can translate it by 'ContentOffsetX' and 'ContentOffsetY'.
                this.contentOffsetTransform = new TranslateTransform();
                this.UpdateTranslationX();
                this.UpdateTranslationY();

                // Setup a transform group to contain the translation and scale transforms, and then
                // assign this to the content's 'RenderTransform'.
                TransformGroup transformGroup = new TransformGroup();
                transformGroup.Children.Add(this.contentOffsetTransform);
                transformGroup.Children.Add(this.contentScaleTransform);
                this.content.RenderTransform = transformGroup;
            }
        }

        /// <summary>
        /// Shift the content offset one page down.
        /// </summary>
        public void PageDown()
        { this.ContentOffsetY += this.ContentViewportHeight; }

        /// <summary>
        /// Shift the content offset one page left.
        /// </summary>
        public void PageLeft()
        { this.ContentOffsetX -= this.ContentViewportWidth; }

        /// <summary>
        /// Shift the content offset one page right.
        /// </summary>
        public void PageRight()
        { this.ContentOffsetX += this.ContentViewportWidth; }

        /// <summary>
        /// Shift the content offset one page up.
        /// </summary>
        public void PageUp()
        { this.ContentOffsetY -= this.ContentViewportHeight; }

        /// <summary>
        /// Instantly scale the content so that it fits completely in the control.
        /// </summary>
        public void ScaleToFit()
        {
            if (this.content == null)
            { throw new ApplicationException("PART_Content was not found in the ZoomAndPanControl visual template!"); }

            this.ZoomTo(new Rect(0, 0, this.content.ActualWidth, this.content.ActualHeight));
        }

        /// <summary>
        /// Called when the offset of the horizontal scrollbar has been set.
        /// </summary>
        /// <param name="offset">The offset.</param>
        public void SetHorizontalOffset(double offset)
        {
            if (this.disableScrollOffsetSync)
            { return; }

            try
            {
                this.disableScrollOffsetSync = true;
                this.ContentOffsetX = offset / this.ContentScale;
            }
            finally
            { this.disableScrollOffsetSync = false; }
        }

        /// <summary>
        /// Called when the offset of the vertical scrollbar has been set.
        /// </summary>
        /// <param name="offset">The offset.</param>
        public void SetVerticalOffset(double offset)
        {
            if (this.disableScrollOffsetSync)
            { return; }

            try
            {
                this.disableScrollOffsetSync = true;
                this.ContentOffsetY = offset / this.ContentScale;
            }
            finally
            { this.disableScrollOffsetSync = false; }
        }

        /// <summary>
        /// Instantly center the view on the specified point (in content coordinates).
        /// </summary>
        /// <param name="contentOffset">Content offset</param>
        public void SnapContentOffsetTo(Point contentOffset)
        {
            this.CancelAnimation(ContentOffsetXProperty);
            this.CancelAnimation(ContentOffsetYProperty);

            this.ContentOffsetX = contentOffset.X;
            this.ContentOffsetY = contentOffset.Y;
        }

        /// <summary>
        /// Instantly center the view on the specified point (in content coordinates).
        /// </summary>
        /// <param name="contentPoint">Content point</param>
        public void SnapTo(Point contentPoint)
        {
            this.CancelAnimation(ContentOffsetXProperty);
            this.CancelAnimation(ContentOffsetYProperty);

            this.ContentOffsetX = contentPoint.X - (this.ContentViewportWidth / 2);
            this.ContentOffsetY = contentPoint.Y - (this.ContentViewportHeight / 2);
        }

        /// <summary>
        /// Zoom in/out centered on the specified point (in content coordinates).
        /// The focus point is kept locked to it's on screen position (a la Google maps).
        /// </summary>
        /// <param name="newContentScale">Content scale</param>
        /// <param name="contentZoomFocus">Content focus</param>
        public void ZoomAboutPoint(double newContentScale, Point contentZoomFocus)
        {
            newContentScale = Math.Min(Math.Max(newContentScale, this.MinContentScale), this.MaxContentScale);

            double screenSpaceZoomOffsetX = (contentZoomFocus.X - this.ContentOffsetX) * this.ContentScale;
            double screenSpaceZoomOffsetY = (contentZoomFocus.Y - this.ContentOffsetY) * this.ContentScale;
            double contentSpaceZoomOffsetX = screenSpaceZoomOffsetX / newContentScale;
            double contentSpaceZoomOffsetY = screenSpaceZoomOffsetY / newContentScale;
            double newContentOffsetX = contentZoomFocus.X - contentSpaceZoomOffsetX;
            double newContentOffsetY = contentZoomFocus.Y - contentSpaceZoomOffsetY;
            this.CancelAnimation(ContentScaleProperty);
            this.CancelAnimation(ContentOffsetXProperty);
            this.CancelAnimation(ContentOffsetYProperty);

            this.ContentScale = newContentScale;
            this.ContentOffsetX = newContentOffsetX;
            this.ContentOffsetY = newContentOffsetY;
        }

        /// <summary>
        /// Instantly zoom to the specified rectangle (in content coordinates).
        /// </summary>
        /// <param name="contentRect">Content area</param>
        public void ZoomTo(Rect contentRect)
        {
            double scaleX = this.ContentViewportWidth / contentRect.Width;
            double scaleY = this.ContentViewportHeight / contentRect.Height;
            double newScale = this.ContentScale * Math.Min(scaleX, scaleY);

            this.ZoomPointToViewportCenter(newScale, new Point(contentRect.X + (contentRect.Width / 2), contentRect.Y + (contentRect.Height / 2)));
        }

        /// <summary>
        /// Zoom in/out centered on the viewport center.
        /// </summary>
        /// <param name="contentScale">Content scale</param>
        public void ZoomTo(double contentScale)
        {
            Point zoomCenter = new Point(this.ContentOffsetX + (this.ContentViewportWidth / 2), this.ContentOffsetY + (this.ContentViewportHeight / 2));
            this.ZoomAboutPoint(contentScale, zoomCenter);
        }

        /// <summary>
        /// Called to arrange and size the content of a <see cref="T:System.Windows.Controls.Control" /> object.
        /// </summary>
        /// <param name="arrangeBounds">The computed size that is used to arrange the content.</param>
        /// <returns>The size of the control.</returns>
        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            Size size = base.ArrangeOverride(this.DesiredSize);

            if (this.content.DesiredSize != this.unScaledExtent)
            {
                // Use the size of the child as the un-scaled extent content.
                this.unScaledExtent = this.content.DesiredSize;

                if (this.scrollOwner != null)
                { this.scrollOwner.InvalidateScrollInfo(); }
            }

            // Update the size of the viewport onto the content based on the passed in 'arrangeBounds'.
            this.UpdateViewportSize(arrangeBounds);

            return size;
        }

        /// <summary>
        /// Called to re-measure a control.
        /// </summary>
        /// <param name="constraint">The maximum size that the method can return.</param>
        /// <returns>The size of the control, up to the maximum specified by <paramref name="constraint" />.</returns>
        protected override Size MeasureOverride(Size constraint)
        {
            Size infiniteSize = new Size(double.PositiveInfinity, double.PositiveInfinity);
            Size childSize = base.MeasureOverride(infiniteSize);

            if (childSize != this.unScaledExtent)
            {
                // Use the size of the child as the un-scaled extent content.
                this.unScaledExtent = childSize;

                if (this.scrollOwner != null)
                { this.scrollOwner.InvalidateScrollInfo(); }
            }

            // Update the size of the viewport onto the content based on the passed in 'constraint'.
            this.UpdateViewportSize(constraint);

            double width = constraint.Width;
            double height = constraint.Height;

            // Make sure we don't return infinity!
            if (double.IsInfinity(width))
            { width = childSize.Width; }

            // Make sure we don't return infinity!
            if (double.IsInfinity(height))
            { height = childSize.Height; }

            this.UpdateTranslationX();
            this.UpdateTranslationY();

            return new Size(width, height);
        }

        /// <summary>
        /// Method called to clamp the 'ContentOffsetX' value to its valid range.
        /// </summary>
        /// <param name="d">The object.</param>
        /// <param name="baseValue">The base value.</param>
        /// <returns>Coerced value</returns>
        private static object ContentOffsetX_Coerce(DependencyObject d, object baseValue)
        {
            ZoomAndPanControl c = (ZoomAndPanControl)d;
            double value = (double)baseValue;
            const double MinOffsetX = 0.0;

            double maxOffsetX = Math.Max(0.0, c.unScaledExtent.Width - c.constrainedContentViewportWidth);
            value = Math.Min(Math.Max(value, MinOffsetX), maxOffsetX);
            return value;
        }

        /// <summary>
        /// Event raised when the 'ContentOffset' property has changed value.
        /// </summary>
        /// <param name="o">The object.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ContentOffset_PropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ZoomAndPanControl c = (ZoomAndPanControl)o;

            if (e.Property == ContentOffsetXProperty)
            {
                c.UpdateTranslationX();

                // Normally want to automatically update content focus when content offset changes.
                // Although this is disabled using 'disableContentFocusSync' when content offset changes due to in-progress zooming.
                if (!c.disableContentFocusSync)
                { c.UpdateContentZoomFocusX(); }

                // Raise an event to let users of the control know that the content offset has changed.
                if (c.ContentOffsetXChanged != null)
                { c.ContentOffsetXChanged(c, EventArgs.Empty); }
            }
            else if (e.Property == ContentOffsetYProperty)
            {
                c.UpdateTranslationY();

                // Normally want to automatically update content focus when content offset changes.
                // Although this is disabled using 'disableContentFocusSync' when content offset changes due to in-progress zooming.
                if (!c.disableContentFocusSync)
                { c.UpdateContentZoomFocusY(); }

                // Raise an event to let users of the control know that the content offset has changed.
                if (c.ContentOffsetYChanged != null)
                { c.ContentOffsetYChanged(c, EventArgs.Empty); }
            }

            // Notify the owning ScrollViewer that the scrollbar offsets should be updated.
            if (!c.disableScrollOffsetSync && c.scrollOwner != null)
            { c.scrollOwner.InvalidateScrollInfo(); }
        }

        /// <summary>
        /// Method called to clamp the 'ContentOffsetY' value to its valid range.
        /// </summary>
        /// <param name="d">The object.</param>
        /// <param name="baseValue">The base value.</param>
        /// <returns>Coerced value</returns>
        private static object ContentOffsetY_Coerce(DependencyObject d, object baseValue)
        {
            ZoomAndPanControl c = (ZoomAndPanControl)d;
            double value = (double)baseValue;
            const double MinOffsetY = 0.0;

            double maxOffsetY = Math.Max(0.0, c.unScaledExtent.Height - c.constrainedContentViewportHeight);
            value = Math.Min(Math.Max(value, MinOffsetY), maxOffsetY);
            return value;
        }

        /// <summary>
        /// Method called to clamp the 'ContentScale' value to its valid range.
        /// </summary>
        /// <param name="d">The object.</param>
        /// <param name="baseValue">The base value.</param>
        /// <returns>Coerced value</returns>
        private static object ContentScale_Coerce(DependencyObject d, object baseValue)
        {
            ZoomAndPanControl c = (ZoomAndPanControl)d;
            double value = (double)baseValue;
            value = Math.Min(Math.Max(value, c.MinContentScale), c.MaxContentScale);
            return value;
        }

        /// <summary>
        /// Event raised when the 'ContentScale' property has changed value.
        /// </summary>
        /// <param name="o">The object.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ContentScale_PropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ZoomAndPanControl c = (ZoomAndPanControl)o;

            if (c.contentScaleTransform != null)
            {
                // Update the content scale transform whenever 'ContentScale' changes.
                c.contentScaleTransform.ScaleX = c.ContentScale;
                c.contentScaleTransform.ScaleY = c.ContentScale;
            }

            // Update the size of the viewport in content coordinates.
            c.UpdateContentViewportSize();

            if (c.enableContentOffsetUpdateFromScale)
            {
                try
                {
                    // Disable content focus synchronization.  We are about to update content offset while zooming
                    // to ensure that the viewport is focused on our desired content focus point. Setting this
                    // to 'true' stops the automatic update of the content focus when content offset changes.
                    c.disableContentFocusSync = true;

                    // While zooming in or out, keep the content offset up-to-date so that the viewport is always
                    // focused on the content focus point (and also so that the content focus is locked to the
                    // viewport focus point - this is how the Google maps style zooming works).
                    double viewportOffsetX = c.ViewportZoomFocusX - (c.ViewportWidth / 2);
                    double viewportOffsetY = c.ViewportZoomFocusY - (c.ViewportHeight / 2);
                    double contentOffsetX = viewportOffsetX / c.ContentScale;
                    double contentOffsetY = viewportOffsetY / c.ContentScale;
                    c.ContentOffsetX = (c.ContentZoomFocusX - (c.ContentViewportWidth / 2)) - contentOffsetX;
                    c.ContentOffsetY = (c.ContentZoomFocusY - (c.ContentViewportHeight / 2)) - contentOffsetY;
                }
                finally
                {
                    c.disableContentFocusSync = false;
                }
            }

            if (c.ContentScaleChanged != null)
            { c.ContentScaleChanged(c, EventArgs.Empty); }

            if (c.scrollOwner != null)
            { c.scrollOwner.InvalidateScrollInfo(); }
        }

        /// <summary>
        /// Event raised 'MinContentScale' or 'MaxContentScale' has changed.
        /// </summary>
        /// <param name="o">The object.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void MinOrMaxContentScale_PropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ZoomAndPanControl c = (ZoomAndPanControl)o;
            c.ContentScale = Math.Min(Math.Max(c.ContentScale, c.MinContentScale), c.MaxContentScale);
        }

        /// <summary>
        /// Zoom to the specified scale and move the specified focus point to the center of the viewport.
        /// </summary>
        /// <param name="newContentScale">Content scale</param>
        /// <param name="contentZoomFocus">Content zoom</param>
        /// <param name="callback">Event callback</param>
        private void AnimatedZoomPointToViewportCenter(double newContentScale, Point contentZoomFocus, EventHandler callback)
        {
            newContentScale = Math.Min(Math.Max(newContentScale, this.MinContentScale), this.MaxContentScale);

            this.CancelAnimation(ContentOffsetXProperty);
            this.CancelAnimation(ContentOffsetYProperty);
            this.CancelAnimation(ViewportZoomFocusXProperty);
            this.CancelAnimation(ViewportZoomFocusYProperty);

            this.ContentZoomFocusX = contentZoomFocus.X;
            this.ContentZoomFocusY = contentZoomFocus.Y;
            this.ViewportZoomFocusX = (this.ContentZoomFocusX - this.ContentOffsetX) * this.ContentScale;
            this.ViewportZoomFocusY = (this.ContentZoomFocusY - this.ContentOffsetY) * this.ContentScale;

            // When zooming about a point make updates to ContentScale also update content offset.
            this.enableContentOffsetUpdateFromScale = true;

            this.StartAnimation(
                ContentScaleProperty,
                newContentScale,
                this.AnimationDuration,
                (o, e) =>
                {
                    this.enableContentOffsetUpdateFromScale = false;

                    if (callback != null)
                    { callback(this, EventArgs.Empty); }
                });

            this.StartAnimation(ViewportZoomFocusXProperty, this.ViewportWidth / 2, this.AnimationDuration);
            this.StartAnimation(ViewportZoomFocusYProperty, this.ViewportHeight / 2, this.AnimationDuration);
        }

        /// <summary>
        /// Reset the viewport zoom focus to the center of the viewport.
        /// </summary>
        private void ResetViewportZoomFocus()
        {
            this.ViewportZoomFocusX = this.ViewportWidth / 2;
            this.ViewportZoomFocusY = this.ViewportHeight / 2;
        }

        /// <summary>
        /// Update the size of the viewport in content coordinates after the viewport size or 'ContentScale' has changed.
        /// </summary>
        private void UpdateContentViewportSize()
        {
            this.ContentViewportWidth = this.ViewportWidth / this.ContentScale;
            this.ContentViewportHeight = this.ViewportHeight / this.ContentScale;

            this.constrainedContentViewportWidth = Math.Min(this.ContentViewportWidth, this.unScaledExtent.Width);
            this.constrainedContentViewportHeight = Math.Min(this.ContentViewportHeight, this.unScaledExtent.Height);

            this.UpdateTranslationX();
            this.UpdateTranslationY();
        }

        /// <summary>
        /// Update the X coordinate of the zoom focus point in content coordinates.
        /// </summary>
        private void UpdateContentZoomFocusX()
        { this.ContentZoomFocusX = this.ContentOffsetX + (this.constrainedContentViewportWidth / 2); }

        /// <summary>
        /// Update the Y coordinate of the zoom focus point in content coordinates.
        /// </summary>
        private void UpdateContentZoomFocusY()
        { this.ContentZoomFocusY = this.ContentOffsetY + (this.constrainedContentViewportHeight / 2); }

        /// <summary>
        /// Update the X coordinate of the translation transformation.
        /// </summary>
        private void UpdateTranslationX()
        {
            if (this.contentOffsetTransform != null)
            {
                double scaledContentWidth = this.unScaledExtent.Width * this.ContentScale;

                if (scaledContentWidth < this.ViewportWidth)
                {
                    // When the content can fit entirely within the viewport, center it.
                    this.contentOffsetTransform.X = (this.ContentViewportWidth - this.unScaledExtent.Width) / 2;
                }
                else
                { this.contentOffsetTransform.X = -this.ContentOffsetX; }
            }
        }

        /// <summary>
        /// Update the Y coordinate of the translation transformation.
        /// </summary>
        private void UpdateTranslationY()
        {
            if (this.contentOffsetTransform != null)
            {
                double scaledContentHeight = this.unScaledExtent.Height * this.ContentScale;

                if (scaledContentHeight < this.ViewportHeight)
                {
                    // When the content can fit entirely within the viewport, center it.
                    this.contentOffsetTransform.Y = (this.ContentViewportHeight - this.unScaledExtent.Height) / 2;
                }
                else
                { this.contentOffsetTransform.Y = -this.ContentOffsetY; }
            }
        }

        /// <summary>
        /// Update the viewport size from the specified size.
        /// </summary>
        /// <param name="newSize">Viewport size</param>
        private void UpdateViewportSize(Size newSize)
        {
            // The viewport is already the specified size.
            if (this.viewport == newSize)
            { return; }

            this.viewport = newSize;

            // Update the viewport size in content coordinates.
            this.UpdateContentViewportSize();

            // Initialize the content zoom focus point.
            this.UpdateContentZoomFocusX();
            this.UpdateContentZoomFocusY();

            // Reset the viewport zoom focus to the center of the viewport.
            this.ResetViewportZoomFocus();

            // Update content offset from itself when the size of the viewport changes.
            // This ensures that the content offset remains properly clamped to its valid range.
            this.ContentOffsetX = this.ContentOffsetX;
            this.ContentOffsetY = this.ContentOffsetY;

            if (this.scrollOwner != null)
            {
                // Tell that owning ScrollViewer that scrollbar data has changed.
                this.scrollOwner.InvalidateScrollInfo();
            }
        }

        /// <summary>
        /// Zoom to the specified scale and move the specified focus point to the center of the viewport.
        /// </summary>
        /// <param name="newContentScale">Content scale</param>
        /// <param name="contentZoomFocus">Content focus</param>
        private void ZoomPointToViewportCenter(double newContentScale, Point contentZoomFocus)
        {
            newContentScale = Math.Min(Math.Max(newContentScale, this.MinContentScale), this.MaxContentScale);

            this.CancelAnimation(ContentScaleProperty);
            this.CancelAnimation(ContentOffsetXProperty);
            this.CancelAnimation(ContentOffsetYProperty);

            this.ContentScale = newContentScale;
            this.ContentOffsetX = contentZoomFocus.X - (this.ContentViewportWidth / 2);
            this.ContentOffsetY = contentZoomFocus.Y - (this.ContentViewportHeight / 2);
        }
    }
}
