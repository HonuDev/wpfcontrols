﻿using System.Windows.Media;

namespace Honu.Controls
{
    /// <summary>
    /// Defines the behavior of the Image Zoom control
    /// </summary>
    public interface IImageZoomControl
    {
        /// <summary>
        /// Gets or sets the image source.
        /// </summary>
        ImageSource ImageSource
        { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show the control.
        /// </summary>
        bool ShowControls
        { get; set; }

        /// <summary>
        /// Gets or sets the interaction mode.
        /// </summary>
        MouseHandlingMode InteractionMode
        { get; set; }

        /// <summary>
        /// Scales the image to fit the extent
        /// </summary>
        void ScaleToFit();

        /// <summary>
        /// Resets the view to actual size.
        /// </summary>
        void ResetView();
    }
}
