﻿namespace Honu.Controls
{
    /// <summary>
    /// Defines the current state of the mouse handling logic.
    /// </summary>
    public enum MouseHandlingMode
    {
        /// <summary>
        /// Not in any special mode.
        /// </summary>
        None,

        /// <summary>
        /// The user panning the viewport.
        /// </summary>
        Panning,

        /// <summary>
        /// The user is zooming in.
        /// </summary>
        ZoomingIn,

        /// <summary>
        /// The user is zooming out.
        /// </summary>
        ZoomingOut,

        /// <summary>
        /// The user is dragging a zoom area field.
        /// </summary>
        DragZooming,
    }
}
