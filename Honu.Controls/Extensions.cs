﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Honu.Controls
{
    /// <summary>
    /// Control extensions
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Starts the animation.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="property">Dependency property.</param>
        /// <param name="toValue">To value.</param>
        /// <param name="durationInSeconds">The duration in seconds.</param>
        /// <param name="completedEvent">The completed event.</param>
        public static void StartAnimation(this UIElement element, DependencyProperty property, double toValue, double durationInSeconds, EventHandler completedEvent = null)
        {
            if (element == null)
            { throw new ArgumentNullException("element"); }

            double fromValue = (double)element.GetValue(property);

            DoubleAnimation animation = new DoubleAnimation()
            {
                From = fromValue,
                To = toValue,
                Duration = TimeSpan.FromSeconds(durationInSeconds)
            };

            animation.Completed += (o, e) =>
            {
                // When the animation has completed bake final value of the animation into the property.
                element.SetValue(property, element.GetValue(property));
                element.CancelAnimation(property);

                if (completedEvent != null)
                { completedEvent(o, e); }
            };

            animation.Freeze();
            element.BeginAnimation(property, animation);
        }

        /// <summary>
        /// Cancels the animation.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="property">Dependency property.</param>
        public static void CancelAnimation(this UIElement element, DependencyProperty property)
        {
            if (element == null)
            { throw new ArgumentNullException("element"); }

            element.BeginAnimation(property, null);
        }
    }
}
