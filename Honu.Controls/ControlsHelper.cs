﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;

namespace Honu.Controls
{
    /// <summary>
    /// Controls Helper
    /// </summary>
    internal class ControlsHelper
    {
        /// <summary>
        /// Design-mode value
        /// </summary>
        private static Nullable<bool> isInDesignMode;

        /// <summary>
        /// Gets a value indicating whether this instance is in design mode.
        /// </summary>
        public static bool IsInDesignMode
        {
            get
            {
                // Populate value
                if (!ControlsHelper.isInDesignMode.HasValue)
                {
                    // Check design-time property
                    ControlsHelper.isInDesignMode = (bool)DependencyPropertyDescriptor.FromProperty(DesignerProperties.IsInDesignModeProperty, typeof(FrameworkElement)).Metadata.DefaultValue;

                    // Check to see if we are running in the VS context
                    if (!ControlsHelper.isInDesignMode.Value && Process.GetCurrentProcess().ProcessName.StartsWith("devenv", StringComparison.OrdinalIgnoreCase))
                    { ControlsHelper.isInDesignMode = true; }
                }

                return ControlsHelper.isInDesignMode.Value;
            }
        }
    }
}