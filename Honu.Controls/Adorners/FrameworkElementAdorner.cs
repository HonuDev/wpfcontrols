﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Honu.Controls.Adorners
{
    /// <summary>
    /// This class is an adorner that allows a FrameworkElement derived class to adorn another FrameworkElement.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", Justification = "Field names.")]
    public class FrameworkElementAdorner : Adorner
    {
        private readonly FrameworkElement child;
        private readonly AdornerPlacement horizontalAdornerPlacement;
        private readonly AdornerPlacement verticalAdornerPlacement;
        private readonly double offsetX;
        private readonly double offsetY;
        private double positionX = double.NaN;
        private double positionY = double.NaN;

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameworkElementAdorner"/> class.
        /// </summary>
        /// <param name="adornedElement">The adorned element.</param>
        /// <param name="adornerChildElement">The adorner child element.</param>
        /// <param name="horizontalAdornerPlacement">The horizontal adorner placement.</param>
        /// <param name="verticalAdornerPlacement">The vertical adorner placement.</param>
        /// <param name="offsetX">The X offset.</param>
        /// <param name="offsetY">The Y offset.</param>
        public FrameworkElementAdorner(
            FrameworkElement adornedElement,
            FrameworkElement adornerChildElement,
            AdornerPlacement horizontalAdornerPlacement = AdornerPlacement.Inside,
            AdornerPlacement verticalAdornerPlacement = AdornerPlacement.Inside,
            double offsetX = 0,
            double offsetY = 0)
            : base(adornedElement)
        {
            this.child = adornerChildElement;
            this.horizontalAdornerPlacement = horizontalAdornerPlacement;
            this.verticalAdornerPlacement = verticalAdornerPlacement;
            this.offsetX = offsetX;
            this.offsetY = offsetY;

            adornedElement.SizeChanged += new SizeChangedEventHandler(this.AdornedElement_SizeChanged);

            this.AddLogicalChild(this.child);
            this.AddVisualChild(this.child);
        }

        /// <summary>
        /// Gets or sets the X position
        /// </summary>
        public double PositionX
        {
            get { return this.positionX; }
            set { this.positionX = value; }
        }

        /// <summary>
        /// Gets or sets the Y position
        /// </summary>
        public double PositionY
        {
            get { return this.positionY; }
            set { this.positionY = value; }
        }

        /// <summary>
        /// Gets the AdornedElement from base class for less type-checking.
        /// </summary>
        public new FrameworkElement AdornedElement
        { get { return base.AdornedElement as FrameworkElement; } }

        /// <summary>
        /// Gets an enumerator for logical child elements of this element.
        /// </summary>
        protected override IEnumerator LogicalChildren
        {
            get
            {
                ArrayList list = new ArrayList();
                list.Add(this.child);
                return (IEnumerator)list.GetEnumerator();
            }
        }

        /// <summary>
        /// Gets the number of visual child elements within this element.
        /// </summary>
        protected override int VisualChildrenCount
        { get { return 1; } }

        /// <summary>
        /// Disconnect the child element from the visual tree so that it may be reused later.
        /// </summary>
        public void DisconnectChild()
        {
            this.RemoveLogicalChild(this.child);
            this.RemoveVisualChild(this.child);
        }

        /// <summary>
        /// When overridden in a derived class, positions child elements and determines a size for a <see cref="T:System.Windows.FrameworkElement" /> derived class.
        /// </summary>
        /// <param name="finalSize">The final area within the parent that this element should use to arrange itself and its children.</param>
        /// <returns>The actual size used.</returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            double x = this.PositionX;
            if (double.IsNaN(x))
            { x = this.DetermineX(); }

            double y = this.PositionY;
            if (double.IsNaN(y))
            { y = this.DetermineY(); }

            double adornerWidth = this.DetermineWidth();
            double adornerHeight = this.DetermineHeight();

            this.child.Arrange(new Rect(x, y, adornerWidth, adornerHeight));
            return finalSize;
        }

        /// <summary>
        /// Overrides <see cref="M:System.Windows.Media.Visual.GetVisualChild(System.Int32)" />, and returns a child at the specified index from a collection of child elements.
        /// </summary>
        /// <param name="index">The zero-based index of the requested child element in the collection.</param>
        /// <returns>The requested child element. This should not return null; if the provided index is out of range, an exception is thrown.</returns>
        protected override Visual GetVisualChild(int index)
        { return this.child; }

        /// <summary>
        /// Implements any custom measuring behavior for the adorner.
        /// </summary>
        /// <param name="constraint">A size to constrain the adorner to.</param>
        /// <returns>A <see cref="T:System.Windows.Size" /> object representing the amount of layout space needed by the adorner.</returns>
        protected override Size MeasureOverride(Size constraint)
        {
            this.child.Measure(constraint);
            return this.child.DesiredSize;
        }

        /// <summary>
        /// Determine the X coordinate of the child.
        /// </summary>
        /// <returns>X coordinate</returns>
        private double DetermineX()
        {
            switch (this.child.HorizontalAlignment)
            {
                case HorizontalAlignment.Left:
                    if (this.horizontalAdornerPlacement.Equals(AdornerPlacement.Outside))
                    { return -this.child.DesiredSize.Width + this.offsetX; }

                    return this.offsetX;

                case HorizontalAlignment.Right:
                    if (this.horizontalAdornerPlacement.Equals(AdornerPlacement.Outside))
                    { return this.AdornedElement.ActualWidth + this.offsetX; }

                    return this.AdornedElement.ActualWidth - this.child.DesiredSize.Width + this.offsetX;

                case HorizontalAlignment.Center:
                    double adornerWidth = this.child.DesiredSize.Width;
                    double adornedWidth = this.AdornedElement.ActualWidth;
                    double x = (adornedWidth / 2) - (adornerWidth / 2);
                    return x + this.offsetX;
            }

            return 0.0;
        }

        /// <summary>
        /// Determine the Y coordinate of the child.
        /// </summary>
        /// <returns>Y coordinate</returns>
        private double DetermineY()
        {
            switch (this.child.VerticalAlignment)
            {
                case VerticalAlignment.Top:
                    if (this.verticalAdornerPlacement.Equals(AdornerPlacement.Outside))
                    { return -this.child.DesiredSize.Height + this.offsetY; }

                    return this.offsetY;

                case VerticalAlignment.Bottom:
                    if (this.verticalAdornerPlacement.Equals(AdornerPlacement.Outside))
                    { return this.AdornedElement.ActualHeight + this.offsetY; }

                    return this.AdornedElement.ActualHeight - this.child.DesiredSize.Height + this.offsetY;

                case VerticalAlignment.Center:
                    double adornerHeight = this.child.DesiredSize.Height;
                    double adornedHeight = this.AdornedElement.ActualHeight;
                    double x = (adornedHeight / 2) - (adornerHeight / 2);
                    return x + this.offsetY;
            }

            return 0.0;
        }

        /// <summary>
        /// Determine the width of the child.
        /// </summary>
        /// <returns>Width value</returns>
        private double DetermineWidth()
        {
            if (!double.IsNaN(this.PositionX))
            { return this.child.DesiredSize.Width; }

            switch (this.child.HorizontalAlignment)
            {
                case HorizontalAlignment.Left:
                case HorizontalAlignment.Right:
                case HorizontalAlignment.Center:
                    return this.child.DesiredSize.Width;

                case HorizontalAlignment.Stretch:
                    return this.AdornedElement.ActualWidth;
            }

            return 0.0;
        }

        /// <summary>
        /// Determine the height of the child.
        /// </summary>
        /// <returns>Height value</returns>
        private double DetermineHeight()
        {
            if (!double.IsNaN(this.PositionY))
            { return this.child.DesiredSize.Height; }

            switch (this.child.VerticalAlignment)
            {
                case VerticalAlignment.Top:
                case VerticalAlignment.Bottom:
                case VerticalAlignment.Center:
                    return this.child.DesiredSize.Height;

                case VerticalAlignment.Stretch:
                    return this.AdornedElement.ActualHeight;
            }

            return 0.0;
        }

        /// <summary>
        /// Handles the SizeChanged event of the AdornedElement control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs"/> instance containing the event data.</param>
        private void AdornedElement_SizeChanged(object sender, SizeChangedEventArgs e)
        { this.InvalidateMeasure(); }
    }
}
