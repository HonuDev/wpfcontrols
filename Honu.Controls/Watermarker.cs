﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;

namespace Honu.Controls
{
    /// <summary>
    /// Class that provides the Watermark attached property
    /// </summary>
    public static class Watermarker
    {
        /// <summary>
        /// Watermark Attached Dependency Property
        /// </summary>
        public static readonly DependencyProperty WatermarkProperty = DependencyProperty.RegisterAttached(
           "Watermark",
           typeof(object),
           typeof(Watermarker),
           new FrameworkPropertyMetadata((object)null, new PropertyChangedCallback(OnWatermarkChanged)));

        /// <summary>
        /// Dictionary of ItemsControls
        /// </summary>
        private static readonly Dictionary<object, ItemsControl> ItemsControls = new Dictionary<object, ItemsControl>();

        /// <summary>
        /// Gets the Watermark property. This dependency property indicates the watermark for the control.
        /// </summary>
        /// <param name="d"><see cref="DependencyObject"/> to get the property from</param>
        /// <returns>The value of the Watermark property</returns>
        public static object GetWatermark(DependencyObject d)
        { return (object)d.GetValue(WatermarkProperty); }

        /// <summary>
        /// Sets the Watermark property. This dependency property indicates the watermark for the control.
        /// </summary>
        /// <param name="d"><see cref="DependencyObject"/> to set the property on</param>
        /// <param name="value">value of the property</param>
        public static void SetWatermark(DependencyObject d, object value)
        { d.SetValue(WatermarkProperty, value); }

        /// <summary>
        /// Handles changes to the Watermark property.
        /// </summary>
        /// <param name="d"><see cref="DependencyObject"/> that fired the event</param>
        /// <param name="e">A <see cref="DependencyPropertyChangedEventArgs"/> that contains the event data.</param>
        private static void OnWatermarkChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Control control = (Control)d;
            control.Loaded += Control_Loaded;

            if (control is TextBox)
            {
                TextBox t = (TextBox)control;
                t.TextChanged += TextBox_TextChanged;
            }

            if (control is PasswordBox)
            {
                PasswordBox p = (PasswordBox)control;
                p.PasswordChanged += PasswordBox_PasswordChanged;
            }

            if (control is ComboBox)
            {
                ComboBox c = (ComboBox)control;
                c.DropDownClosed += ComboBox_DropDownClosed;
                c.DropDownOpened += ComboBox_DropDownOpened;
                c.SelectionChanged += ComboBox_SelectionChanged;
            }

            if (control is ItemsControl && !(control is ComboBox))
            {
                ItemsControl i = (ItemsControl)control;

                // for Items property  
                i.ItemContainerGenerator.ItemsChanged += ItemsChanged;
                ItemsControls.Add(i.ItemContainerGenerator, i);

                // for ItemsSource property  
                DependencyPropertyDescriptor prop = DependencyPropertyDescriptor.FromProperty(ItemsControl.ItemsSourceProperty, i.GetType());
                prop.AddValueChanged(i, ItemsSourceChanged);
            }
        }

        /// <summary>
        /// Handles the PasswordChanged event of the PasswordBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private static void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        { ToggleWatermark(sender as Control); }

        /// <summary>
        /// Handles the SelectionChanged event of the ComboBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private static void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox c = sender as ComboBox;
            if (c != null)
            {
                // Retrieve item content
                ComboBoxItem item = e.AddedItems[0] as ComboBoxItem;
                string content = item != null ? item.Content as string : e.AddedItems[0] as string;

                // Toggle watermark
                if (string.IsNullOrWhiteSpace(content))
                { ShowWatermark(c); }
                else
                { RemoveWatermark(c); }
            }
        }

        /// <summary>
        /// Handles the DropDownOpened event of the ComboBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void ComboBox_DropDownOpened(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            if (ShouldShowWatermark(c))
            { RemoveWatermark(c); }
        }

        /// <summary>
        /// Handles the DropDownClosed event of the ComboBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void ComboBox_DropDownClosed(object sender, EventArgs e)
        { ToggleWatermark(sender as Control); }

        /// <summary>
        /// Handles the TextChanged event of the TextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/> instance containing the event data.</param>
        private static void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        { ToggleWatermark(sender as Control); }

        /// <summary>
        /// Handle the Loaded and LostFocus event on the control
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">A <see cref="RoutedEventArgs"/> that contains the event data.</param>
        private static void Control_Loaded(object sender, RoutedEventArgs e)
        {
            Control control = (Control)sender;
            if (ShouldShowWatermark(control))
            { ShowWatermark(control); }
        }

        /// <summary>
        /// Event handler for the items source changed event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
        private static void ItemsSourceChanged(object sender, EventArgs e)
        {
            ItemsControl c = (ItemsControl)sender;

            if (c.ItemsSource != null)
            { ToggleWatermark(c); }
            else
            { ShowWatermark(c); }
        }

        /// <summary>
        /// Toggles the watermark.
        /// </summary>
        /// <param name="c">The control.</param>
        private static void ToggleWatermark(Control c)
        {
            if (ShouldShowWatermark(c))
            { ShowWatermark(c); }
            else
            { RemoveWatermark(c); }
        }

        /// <summary>
        /// Event handler for the items changed event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">A <see cref="ItemsChangedEventArgs"/> that contains the event data.</param>
        private static void ItemsChanged(object sender, ItemsChangedEventArgs e)
        {
            ItemsControl control;
            if (ItemsControls.TryGetValue(sender, out control))
            {
                if (ShouldShowWatermark(control))
                { ShowWatermark(control); }
                else
                { RemoveWatermark(control); }
            }
        }

        /// <summary>
        /// Remove the watermark from the specified element
        /// </summary>
        /// <param name="control">Element to remove the watermark from</param>
        private static void RemoveWatermark(UIElement control)
        {
            AdornerLayer layer = AdornerLayer.GetAdornerLayer(control);

            // layer could be null if control is no longer in the visual tree
            if (layer != null)
            {
                Adorner[] adorners = layer.GetAdorners(control);
                if (adorners == null)
                { return; }

                foreach (Adorner adorner in adorners)
                {
                    if (adorner is WatermarkAdorner)
                    {
                        adorner.Visibility = Visibility.Hidden;
                        layer.Remove(adorner);
                    }
                }
            }
        }

        /// <summary>
        /// Show the watermark on the specified control
        /// </summary>
        /// <param name="control">Control to show the watermark on</param>
        private static void ShowWatermark(Control control)
        {
            AdornerLayer layer = AdornerLayer.GetAdornerLayer(control);

            // layer could be null if control is no longer in the visual tree
            if (layer != null)
            { layer.Add(new WatermarkAdorner(control, GetWatermark(control))); }
        }

        /// <summary>
        /// Indicates whether or not the watermark should be shown on the specified control
        /// </summary>
        /// <param name="c"><see cref="Control"/> to test</param>
        /// <returns>true if the watermark should be shown; false otherwise</returns>
        private static bool ShouldShowWatermark(Control c)
        {
            ComboBox combo = c as ComboBox;
            if (combo != null)
            { return string.IsNullOrEmpty(combo.Text); }

            TextBox text = c as TextBox;
            if (text != null)
            { return string.IsNullOrEmpty(text.Text); }

            PasswordBox pass = c as PasswordBox;
            if (pass != null)
            { return string.IsNullOrEmpty(pass.Password); }

            ItemsControl items = c as ItemsControl;
            return items == null ? false : items.Items.Count.Equals(0);
        }
    }
}