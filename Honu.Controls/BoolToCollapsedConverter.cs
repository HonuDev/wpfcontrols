﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Honu.Controls
{
    /// <summary>
    /// Boolean to Visibility converter
    /// </summary>
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public sealed class BoolToCollapsedConverter : IValueConverter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BoolToCollapsedConverter"/> class
        /// </summary>
        public BoolToCollapsedConverter()
        {
            // Set defaults
            this.TrueValue = Visibility.Visible;
            this.FalseValue = Visibility.Collapsed;
        }

        /// <summary>
        /// Gets or sets the true value
        /// </summary>
        public Visibility TrueValue
        { get; set; }

        /// <summary>
        /// Gets or sets the false value
        /// </summary>
        public Visibility FalseValue
        { get; set; }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>A converted value. If the method returns null, the valid null value is used.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
            { return this.FalseValue; }

            return (bool)value ? this.TrueValue : this.FalseValue;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>A converted value. If the method returns null, the valid null value is used.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (object.Equals(value, this.TrueValue))
            { return true; }

            if (object.Equals(value, this.FalseValue))
            { return false; }

            return DependencyProperty.UnsetValue;
        }
    }
}
